import scrapy
import json
from aia_parser.items import MembersItem



class directory(scrapy.Spider):
    name = "members"

    start_urls = ["https://vcb.aia.org/members?page%5Bnumber%5D={0}&page%5Bsize%5D=50&q=&sort=name" \
                  "&sort-order=asc".format(str(i)) for i in range(500000)]
    counter = 1

    def parse(self, response):
        json_response = json.loads(response.body_as_unicode())
        for data in json_response["data"]:
            item = MembersItem()
            item["type"] = data["type"]
            for k in data["attributes"].keys():
                try:
                    item[k.replace("-", "_")] = data["attributes"][k]
                except Exception as E:
                    print(str(E))
            print(self.counter)
            self.counter += 1
            yield item
