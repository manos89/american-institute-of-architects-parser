# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DirectoryItem(scrapy.Item):
    firm_name = scrapy.Field()
    type = scrapy.Field()
    firm_staff_size = scrapy.Field()
    office_city = scrapy.Field()
    office_state = scrapy.Field()
    office_country = scrapy.Field()
    office_zip = scrapy.Field()
    nfid_matches = scrapy.Field()
    org_cst_key = scrapy.Field()
    no_of_years_reported = scrapy.Field()

class FirmItem(scrapy.Item):
    type = scrapy.Field()
    firm_id = scrapy.Field()
    firm_rec_no = scrapy.Field()
    firm_name = scrapy.Field()
    firm_code = scrapy.Field()
    add_date = scrapy.Field()
    change_user = scrapy.Field()
    change_date = scrapy.Field()
    firm_url = scrapy.Field()
    firm_eml_address = scrapy.Field()
    firm_phn_number_complete_dn = scrapy.Field()
    primary_contact_recno = scrapy.Field()
    primary_contact_key = scrapy.Field()
    primary_contact_name = scrapy.Field()
    architect_count = scrapy.Field()
    associate_count = scrapy.Field()
    allied_count = scrapy.Field()
    intl_associate_count = scrapy.Field()
    address_key = scrapy.Field()
    address_line_1 = scrapy.Field()
    city = scrapy.Field()
    state = scrapy.Field()
    zip = scrapy.Field()
    country = scrapy.Field()
    address_line_2 = scrapy.Field()
    address_line_3 = scrapy.Field()
    number_of_employees_key = scrapy.Field()
    number_of_employees_code = scrapy.Field()
    cornerstone_partner_ext = scrapy.Field()

class MembersItem(scrapy.Item):
    type = scrapy.Field()
    name = scrapy.Field()
    location = scrapy.Field()
    organization = scrapy.Field()
    chapter = scrapy.Field()
    email = scrapy.Field()
    first_name = scrapy.Field()
    last_name = scrapy.Field()
    middle_name = scrapy.Field()
    designation = scrapy.Field()
    college_of_fellows = scrapy.Field()
    image_url = scrapy.Field()
    ces_user_id = scrapy.Field()
    middle_name = scrapy.Field()
    investiture_year = scrapy.Field()