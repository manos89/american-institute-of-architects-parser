import scrapy
import json
from aia_parser.items import DirectoryItem



class directory(scrapy.Spider):
    name = "directory"

    start_urls = ["https://vcb.aia.org/directory-2030?country=&page%5Bnumber%5D={0}" \
                  "&page%5Bsize%5D=50&q=&sort=firm_name&sort-order=asc&state=".format(str(i)) for i in range(50)]

    def parse(self, response):
        json_response = json.loads(response.body_as_unicode())
        for data in json_response["data"]:
            item = DirectoryItem()
            item["firm_name"] = data["attributes"]["firm_name"]
            item["type"] = data["type"]
            item["firm_staff_size"] = data["attributes"]["firm_staff_size"].replace("-", " to ")
            item["office_city"] = data["attributes"]["office_city"]
            item["office_state"] = data["attributes"]["office_state"]
            item["office_country"] = data["attributes"]["office_country"]
            item["office_zip"] = data["attributes"]["office_zip"]
            item["nfid_matches"] = data["attributes"]["nfid_matches"]
            item["org_cst_key"] = data["attributes"]["org_cst_key"]
            item["no_of_years_reported"] = data["attributes"]["no_of_years_reported"]
            yield item
