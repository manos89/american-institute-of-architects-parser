import scrapy
import json
from aia_parser.items import FirmItem



class directory(scrapy.Spider):
    name = "firms"

    start_urls = ["https://vcb.aia.org/architecture-firms?country=&page%5Bnumber%5D={0}&page%5Bsize%5D=50&" \
                  "q=&sort=name&sort-order=asc&state=".format(str(i)) for i in range(5000)]
    counter = 1

    def parse(self, response):
        json_response = json.loads(response.body_as_unicode())
        for data in json_response["data"]:
            item = FirmItem()
            item["type"] = data["type"]
            for k in data["attributes"].keys():
                try:
                    item[k] = data["attributes"][k]
                except Exception as E:
                    print(str(E))
            print(self.counter)
            self.counter += 1
            yield item
